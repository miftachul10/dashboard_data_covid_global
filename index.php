<?php 

$curl= curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result, true);

$global = $result['Global'];
$temp_NewConfirmed = (string)$global['NewConfirmed'];
$temp_TotalConfirmed = (string)$global['TotalConfirmed'];
$temp_NewDeaths = (string)$global['NewDeaths'];
$temp_TotalDeaths = (string)$global['TotalDeaths'];
$temp_NewRecovered = (string)$global['NewRecovered'];
$temp_TotalRecovered = (string)$global['TotalRecovered'];

$countries = $result['Countries'];

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js" integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw==" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/a540d7261a.js" crossorigin="anonymous"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="asset/css/styles.css">
    <title>Dashboard</title>
    <style>
      body{
          background-color: rgb(22, 22, 22);
      }
      .country{
        overflow-x: hidden;
        overflow-y: auto;
        height: 438px;
      }
      #date{
        text-align: center;
      }
    </style>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <nav class="navbar navbar-expand-lg navbar-light bg-dark">
            <a class="navbar-brand" href="#"><img src="img/img1.png" alt="gagal" width="50px" height="50px"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="#"><h1 class="text-light">WHO Coronavirus Disease (COVID-19) Dashboard</h1></a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </div>
    <div class="container-fluid mt-2">
      <div class="row">
          <div class="col-12 col-md-9 mt-1">
            <div class="container bg-dark">
              <div class="row" id="date"> 
                <div class="col-12 text-light">
                  <h2>New Update</h2>    
                </div>   
                <div class="col-12 col-sm-4">
                  <div class="text-warning">
                    <h2><i class="fas fa-arrow-up"></i> <?php echo number_format($temp_NewConfirmed) ?></h2>
                    <h5 class="card-title">New Confirmed</h5>
                  </div>
                </div>
                <div class="col-12 col-sm-4">
                  <div class="text-danger">
                    <h2><i class="fas fa-arrow-up"></i> <?php echo number_format($temp_NewDeaths) ?></h2>
                    <h5 class="card-title">New Deaths</h5>
                    </div>
                </div>
                <div class="col-12 col-sm-4">                  
                    <div class="text-success">
                      <h2><i class="fas fa-arrow-up"></i> <?php echo number_format($temp_NewRecovered) ?></h2>
                      <h5 class="">New Recovered</h5>
                    </div>
                </div>
              </div>
            </div>            
            <div class="container bg-dark mt-1 text-light">
                <div class="row">
                  <div class="col-12">
                      <h4 style="text-align: center; font-size: 2vw;" class="mt-3">Data Confirmed Covid-19</h4>
                      <div class="d-flex justify-content-center">
                          <canvas width="200" height="90" id="confirm"></canvas>
                      </div>
                  </div>
                  <div class="col-12 col-md-6">
                      <h4 style="text-align: center; font-size: 2vw;" class="mt-3">Data Death Covid-19</h4>
                      <div class="d-flex justify-content-center">
                          <canvas id="death" class="mt-3"></canvas>
                      </div>
                  </div>
                  <div class="col-12 col-md-6">
                      <h4 style="text-align: center; font-size: 2vw;" class="mt-3">Data Recovery Covid-19</h4>
                    <div class="d-flex justify-content-center">
                        <canvas id="recovery" class="mt-3"></canvas>
                    </div>
                  </div>
                </div>
            </div>  
          </div>       
          <div class="col-12 col-md-3 mt-1">
            <div class="bg-dark text-light">
                <h6 class="p-3">Update Globally, as of <br> <?php echo $countries[77]['Date'];?></h6>
            </div>
            <div class="bg-dark" id="date">
                <h2 class="text-light p-1">Global Cases</h2>
                <div >
                    <div class="text-warning p-2">
                        <h3><?php echo number_format($temp_TotalConfirmed) ?></h3>
                        <h5 class="card-title">Total Confirmed</h5>
                    </div>
                </div>
                <div>
                    <div class="text-danger p-2">
                        <h3><?php echo number_format($temp_TotalDeaths) ?></h3>
                        <h5 class="card-title">Total Deaths</h5>
                    </div>
                </div>
                <div>
                    <div class="text-success p-2">
                        <h3><?php echo number_format($temp_TotalRecovered) ?></h3>
                        <h5 class="card-title">Total Recovered</h5>
                    </div>
                </div>
            </div>
            <div class="card-header bg-dark text-info mt-1">
              <h3>Country / Regions</h3>
            </div>
            <div class="country bg-dark">
                <?php foreach($countries as $key=>$value):?>
                <ul  class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link  text-light pl-3" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo $value['Country'];?>
                        </a>
                        <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdown">
                            <p class="dropdown-item text-warning">Confirmed: <?php echo number_format($value['TotalConfirmed']);?></p>
                            <p class="dropdown-item text-danger">Deaths: <?php echo number_format($value['TotalDeaths']);?></p>
                            <p class="dropdown-item text-success">Recovered: <?php echo number_format($value['TotalRecovered']);?></p>
                        </div>
                    </li>
                </ul>
                <?php endforeach;?>
            </div>    
          </div>
       </div>
    </div>
    <script>
        var confirmed = document.getElementById('confirm').getContext('2d');
        var death = document.getElementById('death').getContext('2d');
        var recovery = document.getElementById('recovery').getContext('2d');
        
        var data = $.ajax({
            url: "https://api.covid19api.com/summary",
            cache: false
        })
        .done(function (covid) {
            
            function getCountries(covid) {
                var temp_country=[];
                
                covid.Countries.forEach(function(el){
                    temp_country.push(el.Country);
                })
                return temp_country;
            }
            
            function getConfirmed(covid) {
                var temp_confirmed=[];
                
                covid.Countries.forEach(function(el) {
                    temp_confirmed.push(el.TotalConfirmed)
                })
                return temp_confirmed;
            }
            
            function getDeath(covid) {
                var temp_death = [];
                
                covid.Countries.forEach(function(el) {
                    temp_death.push(el.TotalDeaths)
                })
                return temp_death;
            }

            function getRecovery(covid) {
                var temp_recovery = [];

                covid.Countries.forEach(function(el) {
                    temp_recovery.push(el.TotalRecovered)
                })
                return temp_recovery;
            }
            
            var colors = [];
            function getRandomColor() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            }     
            
            for (var i in covid.Countries) {
                colors.push(getRandomColor());
            }
            
            var confirmPieChart = new Chart(confirmed,{
                type: 'bar',
                data: {
                    labels: getCountries(covid),
                    datasets: [{
                        backgroundColor: colors,
                        label: '# Total',
                        data: getConfirmed(covid),
                        borderWidth: 0.5
                    }]
                }, 
                options: {
                    legend: {
                        display: false,
                    }
                }
            })

            var deathPieChart = new Chart(death,{
                type: 'bar',
                data: {
                    labels: getCountries(covid),
                    datasets: [{
                        backgroundColor: colors,
                        label: '# Total',
                        data: getDeath(covid),
                        borderWidth: 0.5
                    }]
                }, 
                options: {
                    legend: {
                        display: false,
                    }
                }
            })

            var recoveryPieChart = new Chart(recovery,{
                type: 'bar',
                data: {
                    labels: getCountries(covid),
                    datasets: [{
                        backgroundColor: colors,
                        label: '# Total',
                        data: getRecovery(covid),
                        borderWidth:0
                    }]
                }, 
                options: {
                    legend: {
                        display: false,
                    }
                }
            })
        });
    </script>
      
    
    <!-- jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  </body>
</html>